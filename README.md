#"SHARE YOUR STORY" TEMPLATE #

Widget for Guardian NextGen article page – to be added via gdn/cdn embed and weighted "supporting" in page template

Example: http://www.theguardian.com/world/2014/may/28/-sp-struggle-housing-americas-mental-health-care-crisis-care

###Set up###

* Clone Index.html
* Edit sentence & window.open URL (see annotated directions)
* Deploy via gdn-cdn --> embed bucket
* Insert into Composer article template and add image/embed weighting of "supporting" to appear in left hand column

